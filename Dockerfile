FROM php:7.4.9-fpm-alpine3.12
RUN apk upgrade --update && apk add \
    git \
    mysql-client \
    --no-cache $PHPIZE_DEPS \
    && pecl install xdebug-2.9.8 \
    && docker-php-ext-enable xdebug
RUN apk add --no-cache \
            freetype-dev \
            libpng-dev \
            jpeg-dev \
            libjpeg-turbo-dev
RUN docker-php-ext-configure gd --with-freetype --with-jpeg
RUN NUMPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) \
    && docker-php-ext-install -j${NUMPROC} gd
RUN docker-php-ext-install exif
RUN apk add --no-cache curl
RUN pecl install -o -f redis \
&&  rm -rf /tmp/pear \
&&  docker-php-ext-enable redis
RUN apk --update add libxml2-dev
RUN docker-php-ext-install pdo_mysql
RUN docker-php-ext-install mysqli && docker-php-ext-enable mysqli
RUN apk add --no-cache zip libzip-dev
RUN docker-php-ext-configure zip
RUN docker-php-ext-install zip
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
RUN echo "short_open_tag = On" >> /usr/local/etc/php/php.ini
RUN echo "upload_max_filesize = 20M" >> /usr/local/etc/php/php.ini
RUN echo "max_execution_time  = 120" >> /usr/local/etc/php/php.ini

RUN apk add --no-cache nodejs-current
RUN apk add --no-cache npm
RUN apk add --no-cache yarn
COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer
