<?php

namespace App\Jobs;

use App\Http\Controllers\Api\QueueController;
use App\Mail\Mailer;
use App\Models\Customer;
use App\Models\Queue;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $queueResult = Customer::whereRelation('question.queue.status', 'id', '1')
            ->whereRelation('question.queue.status', 'retries', '<', '3')
            ->get();

        foreach($queueResult as $customer) {
            $queueId = $customer->question->queue->id;

            $details = [
                'title'         => 'Lead - ' . $queueId,
                'firstname'     => $customer->firstname,
                'lastname'      => $customer->lastname,
                'phone'         => $customer->phone,
                'email'         => $customer->email,
                'question'      => $customer->question->question,
                'created_at'    => $customer->created_at
            ];

            Mail::to(config('mail.mailers.smtp.to_mail'))->send(new Mailer($details));

            // check for failures
            if (Mail::failures()) {
                $queue = Queue::find($queueId);

                $queue->status_id = 3;
                $queue->retries += 1;

                if($queue->retries > 3) {
                    $queue->status_id = 4;
                }

                $queue->save();
            } else {
                $queue = Queue::find($queueId);

                $queue->status_id = 2;
                $queue->save();
            }
        }
    }
}
