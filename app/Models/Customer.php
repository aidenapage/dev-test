<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    //table name
    protected $table = 'customers';

    //primary key
    public $primaryKey = 'id';

    // time stamp
    public $timeStamps = true;

    protected $fillable = [
        'firstname',
        'lastname',
        'email',
        'market',
        'phone',
        'tandc',
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d',
    ];

    public function setMarketAttribute ($value) {
        $this->attributes['market'] = !empty($value) ? now() : null;
    }

    public function setTandcAttribute ($value) {
        $this->attributes['tandc'] = now();
    }

    public function address() {
        return $this->hasOne(Address::class);
    }

    public function question() {
        return $this->hasOne(Question::class);
    }
}
