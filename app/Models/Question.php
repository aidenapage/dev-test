<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;

    //table name
    protected $table = 'questions';

    protected $fillable = [
        'customer_id',
        'question',
    ];

    public function customer(){
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function queue(){
        return $this->hasOne(Queue::class);
    }
}
