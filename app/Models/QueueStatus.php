<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QueueStatus extends Model
{
    use HasFactory;

    //table name
    protected $table = 'queuestatuses';

    public function queue(){
        return $this->belongsToMany(Queue::class, 'queues', 'status_id');
    }
}
