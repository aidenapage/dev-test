<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory;

    //table name
    protected $table = 'addresses';

    protected $fillable = [
        'customer_id',
        'address',
        'address2',
        'province_id',
        'city_id',
        'zip',
    ];

    public function city(){
        return $this->hasOne(City::class);
    }

    public function province(){
        return $this->hasOne(Province::class);
    }

    public function customer(){
        return $this->belongsTo(Customer::class, 'customer_id');
    }
}
