<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory;

    //table name
    protected $table = 'cities';

    public function address(){
        return $this->belongsTo(Address::class);
    }
}
