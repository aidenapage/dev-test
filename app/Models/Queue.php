<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Queue extends Model
{
    use HasFactory;

    //table name
    protected $table = 'queues';

    protected $fillable = [
        'question_id',
    ];

    protected $guarded = ['retries', 'status_id'];

    public function status(){
        return $this->hasOne(QueueStatus::class, 'id', 'status_id');
    }

    public function question(){
        return $this->belongsTo(Question::class, 'question_id');
    }
}
