@extends('layouts.app')

@section('header')
@endsection

@section('content')
    <h2>Customer List</h2>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">firstname</th>
                <th scope="col">lastname</th>
                <th scope="col">phone</th>
                <th scope="col">email</th>
                <th scope="col">statuses</th>
                <th scope="col">question</th>
                <th scope="col">created_at</th>
            </tr>
            </thead>
            <tbody>
            @foreach($queue as $value)
                <tr>
                    <td>{{$value->id}}</td>
                    <td>{{$value->firstname}}</td>
                    <td>{{$value->lastname}}</td>
                    <td>{{$value->phone}}</td>
                    <td>{{$value->email}}</td>
                    <td>{{$value->question->queue->status->name}}</td>
                    <td>{{ $value->question ? $value->question->question : ''}}</td>
                    <td>{{$value->created_at}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    {{$queue->links('pagination::bootstrap-4')}}

@endsection
