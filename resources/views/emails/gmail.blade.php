<!DOCTYPE html>
<html>
<head>
    <title>lead</title>
</head>
<body>
<h1>{{ $details['title'] }}</h1>
<p>First Name: {{ $details['firstname'] }}</p>
<p>Last Name: {{ $details['lastname'] }}</p>
<p>Contact number: {{ $details['phone'] }}</p>
<p>Email: {{ $details['email'] }}</p>
<p>Question: {{ $details['question'] }}</p>
<p>Created at: {{ $details['created_at'] }}</p>
</body>
</html>
