<div class="container bg-dark-blue mw-100">
    <footer class="py-3">
        <div class="nav justify-content-center border-bottom pb-3 mb-3"></div>
        <div class="text-center mb-5">
            <img class="mb-0 rounded mx-auto d-block" src="/storage/logo-quint-65x65.png" alt="" width="65" height="65">
            <p class="mb-0 text-center text-muted">part of</p>
            <p class="h4 text-white">Quint Group</p>
        </div>
        <p class="text-center text-muted mb-4">© 2018 MONEVO, ALL RIGHTS RESERVED.</p>
    </footer>
</div>
