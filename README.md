<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>


## Edit .env file
- **Add FROM_MAIL**
- **Add TO_MAIL**
## Add to environment variables
- **DB_HOST**
- **DB_USERNAME**
- **DB_PASSWORD**
- **MAIL_USERNAME**
- **MAIL_PASSWORD**
## Command to run
- **composer install**
- **npm install**
- **php artisan migrate:fresh --seed**
- **docker-compose up --build -d**
## Open website url to run artisan
- **/linkstorage**
- **/startscheduler**
