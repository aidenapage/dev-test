#!/bin/sh
npm install && npm run dev
composer install
php artisan storage:link
php artisan migrate:fresh --seed
php artisan schedule:work
