<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('province_id');
            $table->string('name');
            $table->integer('active');
            $table->timestamps();
        });

        $cities = [
            ['province_id' => 1, 'name' => 'Alice', 'active' => 1],
            ['province_id' => 1, 'name' => 'Butterworth', 'active' => 1],
            ['province_id' => 1, 'name' => 'East London', 'active' => 1],
            ['province_id' => 1, 'name' => 'Graaff-Reinet', 'active' => 1],
            ['province_id' => 1, 'name' => 'Grahamstown', 'active' => 1],
            ['province_id' => 1, 'name' => 'King William’s Town', 'active' => 1],
            ['province_id' => 1, 'name' => 'Mthatha', 'active' => 1],
            ['province_id' => 1, 'name' => 'Port Elizabeth', 'active' => 1],
            ['province_id' => 1, 'name' => 'Queenstown', 'active' => 1],
            ['province_id' => 1, 'name' => 'Uitenhage', 'active' => 1],
            ['province_id' => 1, 'name' => 'Zwelitsha', 'active' => 1],
            ['province_id' => 2, 'name' => 'Bethlehem', 'active' => 1],
            ['province_id' => 2, 'name' => 'Bloemfontein', 'active' => 1],
            ['province_id' => 2, 'name' => 'Jagersfontein', 'active' => 1],
            ['province_id' => 2, 'name' => 'Kroonstad', 'active' => 1],
            ['province_id' => 2, 'name' => 'Odendaalsrus', 'active' => 1],
            ['province_id' => 2, 'name' => 'Parys', 'active' => 1],
            ['province_id' => 2, 'name' => 'Phuthaditjhaba', 'active' => 1],
            ['province_id' => 2, 'name' => 'Sasolburg', 'active' => 1],
            ['province_id' => 2, 'name' => 'Virginia', 'active' => 1],
            ['province_id' => 2, 'name' => 'Welkom', 'active' => 1],
            ['province_id' => 3, 'name' => 'Benoni', 'active' => 1],
            ['province_id' => 3, 'name' => 'Boksburg', 'active' => 1],
            ['province_id' => 3, 'name' => 'Brakpan', 'active' => 1],
            ['province_id' => 3, 'name' => 'Carletonville', 'active' => 1],
            ['province_id' => 3, 'name' => 'Germiston', 'active' => 1],
            ['province_id' => 3, 'name' => 'Johannesburg', 'active' => 1],
            ['province_id' => 3, 'name' => 'Krugersdorp', 'active' => 1],
            ['province_id' => 3, 'name' => 'Pretoria', 'active' => 1],
            ['province_id' => 3, 'name' => 'Randburg', 'active' => 1],
            ['province_id' => 3, 'name' => 'Randfontein', 'active' => 1],
            ['province_id' => 3, 'name' => 'Roodepoort', 'active' => 1],
            ['province_id' => 3, 'name' => 'Soweto', 'active' => 1],
            ['province_id' => 3, 'name' => 'Springs', 'active' => 1],
            ['province_id' => 3, 'name' => 'Vanderbijlpark', 'active' => 1],
            ['province_id' => 3, 'name' => 'Vereeniging', 'active' => 1],
            ['province_id' => 4, 'name' => 'Durban', 'active' => 1],
            ['province_id' => 4, 'name' => 'Empangeni', 'active' => 1],
            ['province_id' => 4, 'name' => 'Ladysmith', 'active' => 1],
            ['province_id' => 4, 'name' => 'Newcastle', 'active' => 1],
            ['province_id' => 4, 'name' => 'Pietermaritzburg', 'active' => 1],
            ['province_id' => 4, 'name' => 'Pinetown', 'active' => 1],
            ['province_id' => 4, 'name' => 'Ulundi', 'active' => 1],
            ['province_id' => 4, 'name' => 'Umlazi', 'active' => 1],
            ['province_id' => 5, 'name' => 'Giyani', 'active' => 1],
			['province_id' => 5, 'name' => 'Lebowakgomo', 'active' => 1],
			['province_id' => 5, 'name' => 'Musina', 'active' => 1],
			['province_id' => 5, 'name' => 'Phalaborwa', 'active' => 1],
			['province_id' => 5, 'name' => 'Polokwane', 'active' => 1],
			['province_id' => 5, 'name' => 'Seshego', 'active' => 1],
			['province_id' => 5, 'name' => 'Sibasa', 'active' => 1],
			['province_id' => 5, 'name' => 'Thabazimbi', 'active' => 1],
            ['province_id' => 6, 'name' => 'Emalahleni', 'active' => 1],
			['province_id' => 6, 'name' => 'Nelspruit', 'active' => 1],
            ['province_id' => 6, 'name' => 'Secunda', 'active' => 1],
            ['province_id' => 7, 'name' => 'Kimberley', 'active' => 1],
			['province_id' => 7, 'name' => 'Kuruman', 'active' => 1],
			['province_id' => 7, 'name' => 'Port Nolloth', 'active' => 1],
            ['province_id' => 8, 'name' => 'Klerksdorp', 'active' => 1],
			['province_id' => 8, 'name' => 'Mahikeng', 'active' => 1],
			['province_id' => 8, 'name' => 'Mmabatho', 'active' => 1],
			['province_id' => 8, 'name' => 'Potchefstroom', 'active' => 1],
			['province_id' => 8, 'name' => 'Rustenburg', 'active' => 1],
            ['province_id' => 9, 'name' => 'Bellville', 'active' => 1],
			['province_id' => 9, 'name' => 'Cape Town', 'active' => 1],
			['province_id' => 9, 'name' => 'Constantia', 'active' => 1],
			['province_id' => 9, 'name' => 'George', 'active' => 1],
			['province_id' => 9, 'name' => 'Hopefield', 'active' => 1],
			['province_id' => 9, 'name' => 'Oudtshoorn', 'active' => 1],
			['province_id' => 9, 'name' => 'Paarl', 'active' => 1],
			['province_id' => 9, 'name' => 'Simon’s Town', 'active' => 1],
			['province_id' => 9, 'name' => 'Stellenbosch', 'active' => 1],
			['province_id' => 9, 'name' => 'Swellendam', 'active' => 1],
			['province_id' => 9, 'name' => 'Worcester', 'active' => 1]
        ];

        DB::table('cities')->insert($cities);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
