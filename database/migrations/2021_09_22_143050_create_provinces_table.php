<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateProvincesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provinces', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('active');
            $table->timestamps();
        });

        $provinces = [
            ['name' => 'Eastern Cape', 'active' => 1],
            ['name' => 'Free State', 'active' => 1],
            ['name' => 'Gauteng', 'active' => 1],
            ['name' => 'KwaZulu-Natal', 'active' => 1],
            ['name' => 'Limpopo', 'active' => 1],
            ['name' => 'Mpumalanga', 'active' => 1],
            ['name' => 'Northern Cape', 'active' => 1],
            ['name' => 'North West', 'active' => 1],
            ['name' => 'Western Cape', 'active' => 1]
        ];

        DB::table('provinces')->insert($provinces);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('provinces');
    }
}
