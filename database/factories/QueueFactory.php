<?php

namespace Database\Factories;

use App\Models\Question;
use App\Models\Queue;
use App\Models\QueueStatus;
use Illuminate\Database\Eloquent\Factories\Factory;

class QueueFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Queue::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'status_id' => $this->faker->biasedNumberBetween(1, 4),
            'question_id' => Question::factory(),
            'retries' => $this->faker->biasedNumberBetween(0, 3),
        ];
    }
}
