<?php

namespace Database\Factories;

use App\Models\QueueStatus;
use Illuminate\Database\Eloquent\Factories\Factory;

class QueueStatusFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = QueueStatus::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word(),
            'description' => $this->faker->sentence()
        ];
    }

    public function arrayIndex (int $index) {
        $status = [
            ['name' => 'New', 'description' => 'New Entry'],
            ['name' => 'Success', 'description' => 'Entry Successful Processed'],
            ['name' => 'Retry', 'description' => 'Failed processed - Retry'],
            ['name' => 'failed', 'description' => 'Failed processed - No Retries']
        ];

        return $this->state([
            'name' => $status[$index]['name'],
            'description' => $status[$index]['description']
        ]);
    }
}
