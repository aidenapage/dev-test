<?php

namespace Database\Factories;

use App\Models\Address;
use App\Models\Customer;
use Illuminate\Database\Eloquent\Factories\Factory;

class AddressFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Address::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'customer_id' => Customer::factory(),
            'address' => $this->faker->streetAddress(),
            'address2' => null,
            'province_id' => $this->faker->biasedNumberBetween(1, 9),
            'city_id' => $this->faker->biasedNumberBetween(1, 74),
            'zip' => $this->faker->biasedNumberBetween(1000, 99999),
        ];
    }
}
