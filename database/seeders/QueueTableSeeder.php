<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Queue;

class QueueTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Queue::factory()->count(50)->create();
    }
}
