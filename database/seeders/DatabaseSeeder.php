<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        $this->call(CustomerTableSeeder::class);
//        $this->call(AddressTableSeeder::class);
//        $this->call(QuestionTableSeeder::class);
        $this->call(QueueTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(QueueStatusTableSeeder::class);
    }
}
