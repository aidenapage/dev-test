<?php

namespace Database\Seeders;

use App\Models\QueueStatus;
use Illuminate\Database\Seeder;

class QueueStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        QueueStatus::factory()->arrayIndex(0)->create();
        QueueStatus::factory()->arrayIndex(1)->create();
        QueueStatus::factory()->arrayIndex(2)->create();
        QueueStatus::factory()->arrayIndex(3)->create();
    }
}
