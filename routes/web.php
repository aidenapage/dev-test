<?php

use App\Http\Controllers\Api\CityController;
use App\Http\Controllers\Api\CustomerController;
use App\Http\Controllers\Api\ProvinceController;
use App\Http\Controllers\Api\QueueController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PagesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PagesController::class, 'index']);

// Resource
Route::apiResource('customer', CustomerController::class);
Route::apiResource('queue', QueueController::class);
Route::apiResource('province', ProvinceController::class);
Route::apiResource('city', CityController::class);

Auth::routes();
